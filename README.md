# Active Brownian Particles

## Description

Repository for the project in Active Brownian Particles for the course "Advanced Computational Physics".

## Contents

- All data and figures were generated in the two Jupyter notebooks. These have instructions inside on how to be run and the title indicates the portion of the project they focus on.
- 'Animations' contains some videos showing the evolution of the system. The parameters used are in the title of the video.
- 'Data' contains the data generated from the notebooks and is stored to avoid having to rerun every time. Since the procedure was seed for the random number generators, for equal values of the parameters (specified in the title of the data and in the jupyter notebooks) the same data will be obtained.
- 'Figures 1/2/3' contains the generated figures numbered according to the section in which they enter the final report.
